package LAB_3.ChainOfResponsibility;

public class Painter implements CarRebuild {
    private CarRebuild carRebuild;
    private String painterName;

    public Painter(String painterName) {
        this.painterName = painterName;
    }

    @Override
    public void setCarRebuild(CarRebuild carRebuild) {
        this.carRebuild = carRebuild;
    }

    @Override
    public void create(Car car) {
        System.out.println("Painting the car in "+car.getColor()+" color");
        System.out.println(painterName + " forwards car building to " + carRebuild.getCarRebuild());
        carRebuild.create(car);
    }

    @Override
    public String getCarRebuild() {
        return painterName;
    }
}
