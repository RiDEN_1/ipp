package LAB_3.ChainOfResponsibility;

public class CarDelivery implements CarRebuild {
    private CarRebuild carRebuild;
    private String deliveryGuyName;

    public CarDelivery(String deliveryGuyName) {
        this.deliveryGuyName = deliveryGuyName;
    }

    @Override
    public void setCarRebuild(CarRebuild carRebuild) {
        this.carRebuild = carRebuild;
    }

    @Override
    public void create(Car car) {
            System.out.println("Get car delivered by " + deliveryGuyName);
    }

    @Override
    public String getCarRebuild() {
        return deliveryGuyName;
    }
}
