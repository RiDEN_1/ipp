package LAB_3.ChainOfResponsibility;

public class Motorist implements CarRebuild {
    private CarRebuild carRebuild;
    private String motoristName;

    public Motorist(String motoristName) {
        this.motoristName = motoristName;
    }

    @Override
    public void setCarRebuild(CarRebuild carRebuild) {
        this.carRebuild = carRebuild;
    }

    @Override
    public void create(Car car) {
        System.out.println("Tuning engine to "+car.getHorsepower()+" HP");
        System.out.println(motoristName + " forwards car building to " + carRebuild.getCarRebuild());
        carRebuild.create(car);
    }

    @Override
    public String getCarRebuild() {
        return motoristName;
    }
}
