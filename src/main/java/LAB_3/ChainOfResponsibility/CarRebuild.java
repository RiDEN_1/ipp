package LAB_3.ChainOfResponsibility;

public interface CarRebuild {

    void setCarRebuild(CarRebuild carRebuild);

    void create(Car car);

    String getCarRebuild();
}
