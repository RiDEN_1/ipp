package LAB_3.ChainOfResponsibility;

public class Car {
    private final String engineName;
    private final int horsepower;
    private final String color;

    public Car(String engineName, int horsepower, String color) {
        this.engineName = engineName;
        this.horsepower = horsepower;
        this.color = color;
    }

    public String getEngineName() {
        return engineName;
    }

    public int getHorsepower() {
        return horsepower;
    }

    public String getColor() {
        return color;
    }
}
