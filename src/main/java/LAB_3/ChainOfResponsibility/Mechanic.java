package LAB_3.ChainOfResponsibility;

public class Mechanic implements CarRebuild {

    private CarRebuild carRebuild;
    private String mechanicName;

    public Mechanic(String mechanicName) {
        this.mechanicName = mechanicName;
    }

    @Override
    public void setCarRebuild(CarRebuild carRebuild) {
        this.carRebuild = carRebuild;
    }

    @Override
    public void create(Car car) {
        System.out.println("Putting in "+car.getEngineName()+" engine");
        System.out.println(mechanicName + " forwards car building to " + carRebuild.getCarRebuild());
        carRebuild.create(car);
    }

    @Override
    public String getCarRebuild() {
        return mechanicName;
    }
}
