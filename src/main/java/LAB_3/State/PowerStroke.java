package LAB_3.State;

public class PowerStroke implements State {
    @Override
    public void proceedToState(ICEStroke strokeOfEngine) {
        System.out.println("3rd engine stroke: spark ignites the mixture");
        strokeOfEngine.setState(this);
    }
}

