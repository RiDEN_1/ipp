package LAB_3.State;

public class ICEStroke {
    private State state;

    public ICEStroke() {
        state = null;
    }

    void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }
}