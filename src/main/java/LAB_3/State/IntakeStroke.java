package LAB_3.State;

public class IntakeStroke implements State {
    @Override
    public void proceedToState(ICEStroke strokeOfEngine) {
        System.out.println("1st engine stroke: air with fuel mixture is pulled into cylinder");
        strokeOfEngine.setState(this);
    }
}
