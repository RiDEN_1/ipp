package LAB_3.State;

public class CompressionStroke implements State{
    @Override
    public void proceedToState(ICEStroke strokeOfEngine) {
        System.out.println("2nd engine stroke: AF mixture is compressed");
        strokeOfEngine.setState(this);
    }
}
