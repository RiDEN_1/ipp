package LAB_3.State;

public class ExhaustStroke implements State {
    @Override
    public void proceedToState(ICEStroke strokeOfEngine) {
        System.out.println("4th engine stroke: exhaust gases are leaving the cylinder");
        strokeOfEngine.setState(this);
    }
}
