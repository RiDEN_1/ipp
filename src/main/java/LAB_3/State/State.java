package LAB_3.State;

public interface State {
    void proceedToState(ICEStroke strokeOfEngine);
}