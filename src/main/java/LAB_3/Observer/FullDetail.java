package LAB_3.Observer;

public class FullDetail implements CarDetailer {

    @Override
    public void detail() {
        System.out.println("Perform a full detail");
    }
}
