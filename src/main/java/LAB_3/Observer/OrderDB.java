package LAB_3.Observer;

import java.util.ArrayList;
import java.util.List;

public class OrderDB {
    private List<CarDetailer> carDetailers = new ArrayList<>();
    private int state;

    public void add(CarDetailer g) {
        carDetailers.add(g);
    }

    public void execute() {
        for (CarDetailer carDetailer : carDetailers) {
            carDetailer.detail();
        }
    }
}
