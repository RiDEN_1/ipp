package LAB_3;

import LAB_3.ChainOfResponsibility.*;
import LAB_3.ChainOfResponsibility.Painter;
import LAB_3.Command.*;
import LAB_3.Observer.*;
import LAB_3.State.*;
import LAB_3.Strategy.*;


public class Client {

    public static void main(String[] args) {

        System.out.println("Strategy");
        TuningAtelier tuningAtelier = new TuningAtelier(new Stage1Tuning());
        System.out.println(" HP after stage 1 tuning : " + tuningAtelier.doTuning(100) + "HP");
        tuningAtelier = new TuningAtelier(new Stage2Tuning());
        System.out.println(" HP after stage 2 tuning : " + tuningAtelier.doTuning(100) + "HP");
        tuningAtelier = new TuningAtelier(new Stage3Tuning());
        System.out.println(" HP after stage 3 tuning : " + tuningAtelier.doTuning(100) + "HP");
        System.out.println("\n");

        System.out.println("State");
        ICEStroke engineStroke = new ICEStroke();
        IntakeStroke intakeStroke = new IntakeStroke();
        intakeStroke.proceedToState(engineStroke);
        CompressionStroke compressionStroke = new CompressionStroke();
        compressionStroke.proceedToState(engineStroke);
        PowerStroke powerStroke = new PowerStroke();
        powerStroke.proceedToState(engineStroke);
        ExhaustStroke exhaustStroke = new ExhaustStroke();
        exhaustStroke.proceedToState(engineStroke);
        System.out.println("\n");

        System.out.println("Observer");
        OrderDB orderDataBase = new OrderDB();
        orderDataBase.add(new FullDetail());
        orderDataBase.add(new QuickDetail());
        orderDataBase.execute();
        System.out.println("\n");

        System.out.println("Command");
        Engine engine = new Engine();
        Command engineTurnOnCommand = new TurnToPosition3(engine);
        Command engineTurnOffCommand = new TurnToPosition2(engine);
        IgnitionKey ignitionKey = new IgnitionKey(engineTurnOnCommand);
        ignitionKey.turnKey();
        ignitionKey.setCommand(engineTurnOffCommand);
        ignitionKey.turnKey();
        System.out.println("\n");

        System.out.println("Chain Of Responsibility");
        CarRebuild mechanic = new Mechanic("Mechanic");
        CarRebuild motorist = new Motorist("Motorist");
        CarRebuild painter = new Painter("Painter");
        CarRebuild carDelivery = new CarDelivery("CarDeliveryGuy");
        mechanic.setCarRebuild(motorist);
        motorist.setCarRebuild(painter);
        painter.setCarRebuild(carDelivery);
        Car car = new Car("K20A2", 210, "Blaze Orange Metallic");
        mechanic.create(car);
    }
}