package LAB_3.Strategy;

public class Stage2Tuning implements Tuning {
    @Override
    public int tune(int stockHP) {
        return stockHP + 150;
    }
}
