package LAB_3.Strategy;

public class Stage3Tuning implements Tuning {
    @Override
    public int tune(int stockHP) {
        return stockHP + 300;
    }
}
