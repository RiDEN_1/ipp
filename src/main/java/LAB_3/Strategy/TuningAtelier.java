package LAB_3.Strategy;

public class TuningAtelier {
    private Tuning tuning;

    public TuningAtelier(Tuning tuning) {
        this.tuning = tuning;
    }

    public int doTuning(int priceOfOrder) {
        return tuning.tune(priceOfOrder);
    }
}