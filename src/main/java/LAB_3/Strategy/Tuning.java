package LAB_3.Strategy;

public interface Tuning {
    int tune(int priceOfOrder);
}