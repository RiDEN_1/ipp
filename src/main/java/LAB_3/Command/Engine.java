package LAB_3.Command;

public class Engine {
    void turnOnEngine() {
        System.out.println("The Engine is turned ON");
    }

    void turnOffEngine() {
        System.out.println("The Engine is turned OFF");
    }

}
