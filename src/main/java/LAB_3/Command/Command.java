package LAB_3.Command;

public interface Command {
    void execute();
}
