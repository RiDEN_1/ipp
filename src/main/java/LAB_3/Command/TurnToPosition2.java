package LAB_3.Command;

public class TurnToPosition2 implements Command {

    private Engine engine;

    public TurnToPosition2(Engine engine) {
        this.engine= engine;
    }

    @Override
    public void execute() {
        engine.turnOffEngine();
    }

}
