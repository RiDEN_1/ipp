package LAB_3.Command;

public class IgnitionKey {

    private Command command;

    public IgnitionKey(Command command) {
        this.command = command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    public void turnKey() {
        command.execute();
    }

}