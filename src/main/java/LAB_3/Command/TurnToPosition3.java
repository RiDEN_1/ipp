package LAB_3.Command;

public class TurnToPosition3 implements Command {

    private Engine engine;

    public TurnToPosition3(Engine engine) {
        this.engine = engine;
    }

    @Override
    public void execute() {
        engine.turnOnEngine();
    }

}
