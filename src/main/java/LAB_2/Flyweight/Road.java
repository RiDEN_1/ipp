package LAB_2.Flyweight;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Road {
   private Map<Integer,Car> carDataBase = new HashMap<>();

    public Road() {
        populateRoad();
    }

    private void populateRoad(){
        FlyWeightFactory flyWeightFactory = new FlyWeightFactory();
        for (int i = 0; i<1000; i++){
            int number = (int) (Math.random()*10000);
            carDataBase.put(number,flyWeightFactory.getCar(number));
        }
    }
    public void printNumberOfCars(){
        System.out.println("Number of cars on the road: " + carDataBase.size());
    }
    public void printNumberOfObjects(){
        Set<Car> carSet = new HashSet<>(carDataBase.values());
        System.out.println("Number of car objects created: " + carSet.size());
    }
}
