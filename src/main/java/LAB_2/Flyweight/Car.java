package LAB_2.Flyweight;

class Car {
    private final String make;

     Car(String make) {
        this.make = make;
    }
}
