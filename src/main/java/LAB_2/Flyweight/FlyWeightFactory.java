package LAB_2.Flyweight;

import java.util.HashMap;
import java.util.Map;

class FlyWeightFactory {
     private Map<String,Car> carMap = new HashMap<>();
     Car getCar(int licencePlateNumber){
        switch (licencePlateNumber%3){
            case 0:
                return getCarModel("BMW");
            case 1:
                return getCarModel("Audi");
            case 2:
                return getCarModel("Mercedes");
        }
        return null;
    }

    private Car getCarModel(String make) {
         if(!carMap.containsKey(make)){
             Car car=null;
             switch (make){
                 case "BMW":
                     car = new BMW();
                     break;
                 case "Audi":
                     car = new Audi();
                     break;
                 case "Mercedes":
                     car = new Mercedes();
                     break;
             }
             carMap.put(make,car);
        }
        return carMap.get(make);
    }
}
