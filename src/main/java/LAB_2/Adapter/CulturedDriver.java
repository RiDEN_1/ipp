package LAB_2.Adapter;

public class CulturedDriver {

    public void turnRightCultured() {
        System.out.println("Move turn signal up");
    }
    public void turnLeftCultured() {
        System.out.println("Move turn signal down");
    }
}
