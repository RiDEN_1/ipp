package LAB_2.Adapter;

public class UnculturedDriver {
    public void turnRight() {
        System.out.println("Just turn right without notifying");
    }
    public void turnLeft() {
        System.out.println("Just turn left without notifying");
    }
}
