package LAB_2.Adapter;

public class UnCulturedDriverAdapter extends UnculturedDriver{
    private final CulturedDriver culturedDriver;

    public UnCulturedDriverAdapter() {
        culturedDriver = new CulturedDriver();
    }
    @Override
    public void turnRight() {
        culturedDriver.turnRightCultured();
    }
    @Override
    public void turnLeft() {
        culturedDriver.turnLeftCultured();
    }
}
