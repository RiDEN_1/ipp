package LAB_2;
import LAB_2.Adapter.*;
import LAB_2.Bridge.*;
import LAB_2.Decorator.BodyKitAdder;
import LAB_2.Decorator.Car;
import LAB_2.Decorator.SpoilerAdder;
import LAB_2.Decorator.Tuning;
import LAB_2.Facade.CarFacade;
import LAB_2.Flyweight.*;

public class Client {

    public static void main(String[] args) {

        System.out.println("\nBridge:");
        BodyKitParts sideSkirts = new SideSkirts(new Plastic());
        sideSkirts.pickMaterialForPart();
        BodyKitParts frontLip = new FrontLip(new CarbonFiber());
        frontLip.pickMaterialForPart();
        BodyKitParts spoiler = new Spoiler(new FiberGlass());
        spoiler.pickMaterialForPart();

        System.out.println("\nAdapter:");
        UnculturedDriver unculturedDriver = new UnculturedDriver();
        UnCulturedDriverAdapter unCulturedDriverAdapter = new UnCulturedDriverAdapter();
        System.out.println("Uncultured:");
        unculturedDriver.turnLeft();
        unculturedDriver.turnRight();
        System.out.println("Uncultured but adapted");
        unCulturedDriverAdapter.turnLeft();
        unCulturedDriverAdapter.turnRight();

        System.out.println("\nDecorator:");
        Tuning tuning = new BodyKitAdder(new SpoilerAdder(new Car("Acura","RSX")));
        tuning.tune();

        System.out.println("\nFacade:");
        CarFacade car = new CarFacade();
        car.start();

        System.out.println("\nFlyWeight:");
        Road road = new Road();
        road.printNumberOfObjects();
        road.printNumberOfCars();

    }
}