package LAB_2.Bridge;

public class SideSkirts extends BodyKitParts {

    public SideSkirts(Material material) {
        super(material);
    }

    @Override
    public void pickMaterialForPart() {
        System.out.print("Side skirts made of ");
        material.makePartFromMaterial();
    }
}
