package LAB_2.Bridge;

public class FrontLip extends BodyKitParts {

    public FrontLip(Material material) {
        super(material);
    }

    @Override
    public void pickMaterialForPart() {
        System.out.print("Front lip made of ");
        material.makePartFromMaterial();
    }
}
