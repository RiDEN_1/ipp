package LAB_2.Bridge;

public class Spoiler extends BodyKitParts {

    public Spoiler(Material material) {
        super(material);
    }

    @Override
    public void pickMaterialForPart() {
        System.out.print("Spoiler made of ");
        material.makePartFromMaterial();
    }
}
