package LAB_2.Bridge;

public abstract class BodyKitParts {
    Material material;

    BodyKitParts(Material material) {
        this.material = material;
    }

    abstract public void pickMaterialForPart();
}