package LAB_2.Facade;


 class Battery {
     private double voltage;

      Battery() {
         this.voltage = 10 + Math.random()*(14-10);
     }

     double getVoltage() {
        return voltage;
    }
}
