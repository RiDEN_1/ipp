package LAB_2.Facade;

class ComplexCar {
    private Engine engine = new Engine();
    private FuelSystem fuelSystem = new FuelSystem();
    private Battery battery = new Battery();

    boolean checkBattery() {
        return battery.getVoltage() > 12;
    }

    boolean checkFuelSystem() {
        return fuelSystem.checkFuelSystem();
    }

    boolean checkEngine() {
        return engine.checkEngine();
    }
}
