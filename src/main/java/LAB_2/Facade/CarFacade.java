package LAB_2.Facade;

public class CarFacade {
    private ComplexCar car = new ComplexCar();

    public void start() {
        boolean isBatteryOK = car.checkBattery();
        boolean isFuelOK = car.checkFuelSystem();
        boolean isEngineOK = car.checkEngine();
        if (isBatteryOK && isFuelOK && isEngineOK) {
            System.out.println("Car started successfully");
        } else {
            System.out.println("Car failed to start");
        }
    }
}
