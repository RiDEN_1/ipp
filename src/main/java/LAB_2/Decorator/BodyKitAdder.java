package LAB_2.Decorator;

public class BodyKitAdder extends Tuner {
    public BodyKitAdder(Tuning tuning) {
        super(tuning);
    }

    public void tune() {
        super.tune();
        System.out.println("Add a wide bodykit");
    }
}