package LAB_2.Decorator;

abstract class Tuner implements Tuning {
    private Tuning tuning;

    Tuner(Tuning tuning) {
        this.tuning = tuning;
    }

    public void tune() {
        tuning.tune();
    }
}