package LAB_2.Decorator;

public class SpoilerAdder extends Tuner {
    public SpoilerAdder(Tuning tuning) {
        super(tuning);
    }
    public void tune() {
        super.tune();
        System.out.println("Add a spoiler");
    }
}