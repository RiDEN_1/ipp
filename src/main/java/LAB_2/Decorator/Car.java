package LAB_2.Decorator;

public class Car implements Tuning {
    private String make;
    private String model;

    public Car(String make, String model) {
        this.make = make;
        this.model = model;
    }

    @Override
    public void tune() {
        System.out.println("Car" +
                "\nMake: " + make +
                "\nModel: " + model +
                "\nAvailable Tuning:");
    }
}