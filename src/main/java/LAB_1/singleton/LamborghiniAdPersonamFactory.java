package LAB_1.singleton;

public class LamborghiniAdPersonamFactory {

    private static final LamborghiniAdPersonamFactory adPersonamFactory;

    //static block initialization for exception handling
    static{
        try {
            adPersonamFactory =new LamborghiniAdPersonamFactory() ;
        }catch (Exception e){
            throw new RuntimeException("Exception occured in creating singleton instance");
        }
    }

    private LamborghiniAdPersonamFactory() {
    }


    public static LamborghiniAdPersonamFactory getInstance() {
        return adPersonamFactory;
    }


    public void messageOfAdPersonam() {
        System.out.println("Lamborghini launches the Ad Personam Studio at its historic Sant'Agata Bolognese headquarters");
    }
}