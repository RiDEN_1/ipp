package LAB_1;

import LAB_1.prototype.*;
import LAB_1.singleton.*;
import LAB_1.factory_method.*;
import LAB_1.abstract_factory.*;
import LAB_1.builder.*;

public class Client {
    public static void main(String[] args) {
// --------------------------------------------------------------------------------------------------------------
        System.out.println("\nSingleton:");
        LamborghiniAdPersonamFactory website = LamborghiniAdPersonamFactory.getInstance();
        website.messageOfAdPersonam();
// --------------------------------------------------------------------------------------------------------------
        System.out.println("\nFactory Method:");
        BodyType car = CarBodyTypeFactory.getConfigForType("coupe");
        car.configure("3", "RWD");
        car = CarBodyTypeFactory.getConfigForType("SUV");
        car.configure("5", "4WD");
        car = CarBodyTypeFactory.getConfigForType("sedan");
        car.configure("5", "FWD");
// --------------------------------------------------------------------------------------------------------------
        System.out.println("\nAbstract Factory:");
        TimeReducerFactory factory1 = ResultImprover.getFactory("electric");
        PowerIncreasing electric1 = factory1.increasePower();
        electric1.getPercentage(15);
        WeightReducing electric2 = factory1.reduceWeight();
        electric2.getLocation("lowest to the ground");
        TimeReducerFactory factory2 = ResultImprover.getFactory("fuel");
        WeightReducing fuel1 = factory2.reduceWeight();
        fuel1.getLocation("upper part of the car");
        PowerIncreasing fuel2 = factory2.increasePower();
        fuel2.getPercentage(50);
// --------------------------------------------------------------------------------------------------------------
        System.out.println("\nBuilder:");
        Motorist motorist = new Motorist();
        EngineBuilder dieselEngineBuilder = new DieselEngineBuilder();
        EngineBuilder racingEngineBuilder = new RacingEngineBuilder();
        EngineBuilder gasolineEngineBuilder = new GasolineEngineBuilder();

        motorist.setEngineBuilder(dieselEngineBuilder);
        motorist.constructMotor();
        Engine engine1 = motorist.getMotor();
        engine1.printMotor();

        motorist.setEngineBuilder(racingEngineBuilder);
        motorist.constructMotor();
        Engine engine2 = motorist.getMotor();
        engine2.printMotor();

        motorist.setEngineBuilder(gasolineEngineBuilder);
        motorist.constructMotor();
        Engine engine3 = motorist.getMotor();
        engine3.printMotor();
// --------------------------------------------------------------------------------------------------------------
        System.out.println("\nPrototype:");
        JDMstore.getEngine("K20A").addEngine();
        JDMstore.getEngine("K20A3").addEngine();
        JDMstore.getEngine("K20Z1").addEngine();
        JDMstore.getEngine("K20Z1").addEngine();
        JDMstore.getEngine("K20A3").addEngine();
    }
}