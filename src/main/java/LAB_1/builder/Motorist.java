package LAB_1.builder;

public class Motorist {
    private EngineBuilder engineBuilder;

    public void setEngineBuilder(EngineBuilder engineBuilder) {
        this.engineBuilder = engineBuilder;
    }

    public Engine getMotor() {
        return engineBuilder.getEngine();
    }

    public void constructMotor() {
        engineBuilder.makeNewMotor();
        engineBuilder.buildName();
        engineBuilder.buildFueltype();
        engineBuilder.buildPower();
        engineBuilder.buildConfiguration();
    }
}