package LAB_1.builder;

public abstract class EngineBuilder {
    protected Engine engine;

    public Engine getEngine() {
        return engine;
    }

    public void makeNewMotor() {
        engine = new Engine();
    }

    public abstract void buildName();

    public abstract void buildFueltype();

    public abstract void buildPower();

    public abstract void buildConfiguration();

}