package LAB_1.builder;

public class GasolineEngineBuilder extends EngineBuilder {

    @Override
    public void buildName() {
        engine.setName("Gasoline Engine");
    }

    @Override
    public void buildFueltype() {
        engine.setFueltype("E85");
    }

    @Override
    public void buildPower() {
        engine.setPower(560);
    }

    @Override
    public void buildConfiguration() {
        engine.setConfiguration("V10");
    }

}
