package LAB_1.builder;

public class DieselEngineBuilder extends EngineBuilder {

    @Override
    public void buildName() {
        engine.setName("Turbocharged Diesel Engine");
    }

    @Override
    public void buildFueltype() {
        engine.setFueltype("Diesel");
    }

    @Override
    public void buildPower() {
        engine.setPower(400);
    }

    @Override
    public void buildConfiguration() {
        engine.setConfiguration("V6");
    }
}
