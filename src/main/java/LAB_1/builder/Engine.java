package LAB_1.builder;

public class Engine {
    private String name;
    private String fueltype;
    private int power;
    private String configuration;

    public void setName(String name) {
        this.name = name;
    }

    public void setFueltype(String fueltype) {
        this.fueltype = fueltype;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public void setConfiguration(String configuration) {
        this.configuration = configuration;
    }

    public String getFueltype() {
        return fueltype;
    }

    public int getPower() {
        return power;
    }

    public String getConfiguration() {
        return configuration;
    }


    public String getName() {
        return name;
    }

    public void printMotor() {
        System.out.println(name + ":\n" +
                " Fuel Type: " + fueltype + "\n" +
                " Power: " + power + "\n" +
                " Configuration: " + configuration + "\n");
    }
}
