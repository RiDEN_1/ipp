package LAB_1.builder;

public class RacingEngineBuilder extends EngineBuilder {

    @Override
    public void buildName() {
        engine.setName("Top Fuel Engine");
    }

    @Override
    public void buildFueltype() {
        engine.setFueltype("Nitromethane");
    }

    @Override
    public void buildPower() {
        engine.setPower(2000);
    }

    @Override
    public void buildConfiguration() {
        engine.setConfiguration("V8");
    }

}
