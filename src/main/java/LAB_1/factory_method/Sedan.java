package LAB_1.factory_method;

public class Sedan implements BodyType {
    public void configure(String numOfDoors, String drivetrain) {
        System.out.println("Sedan has " + numOfDoors + " doors and has a drivetrain " + drivetrain);
    }
}
