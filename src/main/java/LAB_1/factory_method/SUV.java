package LAB_1.factory_method;

public class SUV implements BodyType {
    public void configure(String numOfDoors, String drivetrain) {
        System.out.println("SUV has " + numOfDoors + " doors and has a drivetrain: " + drivetrain);
    }
}

