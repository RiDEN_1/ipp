package LAB_1.factory_method;

public class Coupe implements BodyType {
    public void configure(String numOfDoors, String drivetrain) {
        System.out.println("Coupe has " + numOfDoors + " doors and has a drivetrain: " + drivetrain);
    }
}
