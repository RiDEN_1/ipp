package LAB_1.factory_method;

public class CarBodyTypeFactory {
    public static BodyType getConfigForType(String bodyType) {
        if (bodyType.equals("coupe"))
            return new Coupe();
        else if (bodyType.equals("sedan"))
            return new Sedan();
        else if (bodyType.equals("SUV"))
            return new SUV();
        return null;
    }
}
