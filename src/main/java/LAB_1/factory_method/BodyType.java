package LAB_1.factory_method;

public interface BodyType {
    void configure(String numOfDoors, String drivetrain);
}