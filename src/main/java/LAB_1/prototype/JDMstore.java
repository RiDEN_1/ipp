package LAB_1.prototype;

import java.util.HashMap;
import java.util.Map;

public class JDMstore {

    private static Map<String, K20> engineMap = new HashMap<String, K20>();

    static {
        engineMap.put("K20A", new K20A());
        engineMap.put("K20A3", new K20A3());
        engineMap.put("K20Z1", new K20Z1());
    }

    public static K20 getEngine(String engineName) {
        return (K20) engineMap.get(engineName).clone();
    }
}