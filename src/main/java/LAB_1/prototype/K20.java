package LAB_1.prototype;

public abstract class K20 implements Cloneable {

    String engineType;
    int power;

    public abstract void addEngine();

    public Object clone() {
        Object clone = null;
        try {
            clone = super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }

    @Override
    public String toString() {
        return ("Name: " + engineType + "\n" +
                "Power: " + power + "\n");
    }
}
