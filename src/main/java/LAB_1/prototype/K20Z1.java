package LAB_1.prototype;

public class K20Z1 extends K20 {

    public K20Z1() {
        this.engineType = "K20Z1";
        this.power = 212;
    }

    @Override
    public void addEngine() {
        System.out.println("K20Z1 made its way to the swap warehouse\n" +
                "power: " + power);
    }
}
