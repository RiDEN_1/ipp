package LAB_1.prototype;

public class K20A extends K20 {

    public K20A() {
        this.engineType = "K20A";
        this.power = 220;
    }

    @Override
    public void addEngine() {
        System.out.println("K20A made its way to the swap warehouse\n" +
                "power:" + power);
    }
}

