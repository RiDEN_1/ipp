package LAB_1.prototype;

public class K20A3 extends K20 {

    public K20A3() {
        this.engineType = "K20A3";
        this.power = 170;
    }

    @Override
    public void addEngine() {
        System.out.println("K20A3 made its way to the swap warehouse\n" +
                "power:" + power);
    }
}
