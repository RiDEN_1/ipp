package LAB_1.abstract_factory;

public class LithiumBatteries implements WeightReducing {

    LithiumBatteries() {
        System.out.println(" Lithium Batteries are much lighter than other types of batteries");
    }

    public void getLocation(String location) {
        System.out.println("Best location for them is " + location);
    }
}
