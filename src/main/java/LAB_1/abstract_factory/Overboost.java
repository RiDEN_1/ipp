package LAB_1.abstract_factory;

public class Overboost implements PowerIncreasing {

    Overboost() {
        System.out.println(" Overboost is an easy way to get more power");
    }

    public void getPercentage(int percentage) {
        System.out.println("It is possible to gain " + percentage + "% more power");
    }
}
