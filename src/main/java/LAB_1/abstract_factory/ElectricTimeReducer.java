package LAB_1.abstract_factory;

class ElectricTimeReducer extends TimeReducerFactory {
    @Override
    public WeightReducing reduceWeight() {
        return new LithiumBatteries();
    }

    @Override
    public PowerIncreasing increasePower() {
        return new Overvoltage();
    }
}
