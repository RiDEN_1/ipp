package LAB_1.abstract_factory;

public class Overvoltage implements PowerIncreasing {

    Overvoltage() {
        System.out.println(" Overvoltage is a way to increase power in electric vehicles");
    }

    public void getPercentage(int percentage) {
        System.out.println("It is possible to gain " + percentage + "% more power");
    }
}
