package LAB_1.abstract_factory;

public interface WeightReducing {
    void getLocation(String location);
}
