package LAB_1.abstract_factory;

public class ResultImprover {
    private static TimeReducerFactory factory = null;

    public static TimeReducerFactory getFactory(String choice) {
        if (choice.equals("electric")) {
            factory = new ElectricTimeReducer();
        } else if (choice.equals("fuel")) {
            factory = new FuelTimeReducer();
        }
        return factory;
    }
}
