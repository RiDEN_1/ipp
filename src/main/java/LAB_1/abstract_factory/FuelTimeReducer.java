package LAB_1.abstract_factory;

class FuelTimeReducer extends TimeReducerFactory {
    @Override
    public WeightReducing reduceWeight() {
        return new StrippingInterior();
    }

    @Override
    public PowerIncreasing increasePower() {
        return new Overboost();
    }
}
