package LAB_1.abstract_factory;

public abstract class TimeReducerFactory {
    public abstract PowerIncreasing increasePower();
    public abstract WeightReducing reduceWeight();
}
