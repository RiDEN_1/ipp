package LAB_1.abstract_factory;

public interface PowerIncreasing {
    void getPercentage(int percentage);
}

