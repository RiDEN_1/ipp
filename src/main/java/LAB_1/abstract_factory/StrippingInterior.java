package LAB_1.abstract_factory;

public class StrippingInterior implements WeightReducing {

    StrippingInterior() {
        System.out.println(" Stripping Interior is an easy way to reduce weight");
    }


    public void getLocation(String location) {
        System.out.println("It is best to strip the interior in the " + location);
    }

}
